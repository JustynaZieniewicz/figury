package figury;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Path2D;

/**
 * Created by Latitude on 2017-12-12.
 */
public class Choinka extends Figura {
    private static final int WIDTH = 30;
    private static final int HEIGHT = 20;
    private static final int INDENT = 10;
    private static final int INDENT2 = 5;
    public Choinka(Graphics2D buf, int del, int w, int h) {
        super(buf, del, w, h);
        Path2D choinka = new Path2D.Float();
        choinka.moveTo(0,0);
        choinka.lineTo(WIDTH, 0);
        choinka.lineTo(WIDTH - INDENT, HEIGHT);
        choinka.lineTo(WIDTH - INDENT2, HEIGHT);
        choinka.lineTo(WIDTH/2, 2*HEIGHT);
        choinka.lineTo(INDENT2, HEIGHT);
        choinka.lineTo(INDENT, HEIGHT);
        choinka.lineTo(0,0);
        choinka.closePath();

        shape = choinka;
        aft = new AffineTransform();
        area = new Area(shape);
    }
}
