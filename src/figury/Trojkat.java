package figury;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Path2D;

/**
 * Created by Latitude on 2017-12-12.
 */
public class Trojkat extends Figura {
    private static final int SIZE = 15;

    public Trojkat(Graphics2D buf, int del, int w, int h) {
        super(buf, del, w, h);
        Path2D trianglePath = new Path2D.Float();
        trianglePath.moveTo(0,0);
        trianglePath.lineTo(SIZE / 2, Math.sqrt(3)/2*SIZE);
        trianglePath.lineTo(SIZE, 0);
        trianglePath.lineTo(0,0);
        trianglePath.closePath();

        shape = trianglePath;
        aft = new AffineTransform();
        area = new Area(shape);
    }

}
